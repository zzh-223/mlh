# -*- coding: UTF-8 -*-

# # l = ['a', 'b', 'c', 'd', 'e','f']
# # print (l)
# # print(l[:-1])
# # print(l[1:])
# #打印列表
# # print (list(zip(l[:-1],l[1:])))
# a1 = [1, 2, 3]
# a2 = a1
# print(id(a1), id(a2))  # id 相同
# # 所以现在a2、a1并不是同一对象的两个引用了，a2变化a1不会改变
# a2 = a2 + [4] # 这个等式中，右边的a2还是和a1的id一样的，一旦赋值成功，a2就指向新的对象
# print(id(a1), id(a2))  # a1的未变化，a2的id变化了
# print(a1) # [1, 2, 3]没有变 id 也不变

