from django.db import models

# Create your models here.
from users.models import User


class BaseModel(models.Model):
    """定义一个记录记录修改和更新时间的类，在迁移时并不会创建"""
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    # update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    class Meta():
        abstract = True  # 说明是抽象模型类, 用于继承使用，数据库迁移时不会创建BaseModel的表


class Tsukkomi(BaseModel):
    """用户吐槽"""
    content = models.TextField(verbose_name='吐槽内容')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='发布用户')
    image_url = models.CharField(max_length=200, default='', null=True, blank=True, verbose_name='图片链接地址')
    share_count = models.IntegerField(default=0, verbose_name='分享次数')
    url = models.CharField(max_length=200, default='', null=True, blank=True, verbose_name='详情链接地址')

    class Meta:
        db_table = 'tb_tsukkomi'
        verbose_name = '用户吐槽'
        verbose_name_plural = verbose_name


class Comment(BaseModel):
    """吐槽评论"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='评论用户')
    tsukkomi = models.ForeignKey(Tsukkomi, on_delete=models.CASCADE, verbose_name='吐槽')
    message = models.CharField(max_length=256, verbose_name='评论')
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, verbose_name='父类别')

    class Meta:
        db_table = 'tb_comment'
        verbose_name = '评论吐槽'
        verbose_name_plural = verbose_name


class TsukkomiLike(models.Model):
    """用户点赞吐槽"""
    tsukkomi = models.ForeignKey(Tsukkomi, on_delete=models.CASCADE, verbose_name='吐槽点赞')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='点赞用户')

    class Meta:
        db_table = 'tb_tsukkomilike'
        verbose_name = '点赞吐槽'
        verbose_name_plural = verbose_name


class TsukkomiCollect(models.Model):
    """用户收藏吐槽"""
    tsukkomi = models.ForeignKey(Tsukkomi, on_delete=models.CASCADE, verbose_name='收藏吐槽')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户收藏吐槽')

    class Meta:
        db_table = 'tb_tsukkomicollect'
        verbose_name = '收藏吐槽'
        verbose_name_plural = verbose_name


class CommentLike(models.Model):
    """用户点赞评论"""
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, verbose_name='用户评论')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='点赞用户')

    class Meta:
        db_table = 'tb_commentlike'
        verbose_name = '点赞评论'
        verbose_name_plural = verbose_name
