from django.apps import AppConfig


class TsukkomiConfig(AppConfig):
    name = 'tsukkomi'
