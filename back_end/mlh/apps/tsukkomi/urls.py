from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^tsukkomi/$', views.TsukkomiListView.as_view()), # 获取吐槽列表
    url(r'^tsukkomi/(?P<pk>\d+)/$', views.TsukkomiInfoView.as_view()), # 吐槽详情
    url(r'^comment/(?P<pk>\d+)/$', views.TsukkomiCommentView.as_view()), # 获取评论
    url(r'^creation/comment/$', views.CreationCommentView.as_view()), # 创建评论
    url(r'^creation/tsukkomi/$', views.CreationTsukkomiView.as_view()), # 发布吐槽
    url(r'^collect/tsukkomi/$', views.TsukkomiCollectLikeView.as_view(),name="tsukkomi_collect"), # 收藏吐槽
    url(r'^like/tsukkomi/$', views.TsukkomiCollectLikeView.as_view(),name="tsukkomi_like"), # 点赞吐槽
    url(r'^like/comment/$', views.CommentLikeView.as_view(),name="comment_like"), # 点赞评论
    url(r'^share/tsukkomi/$', views.ShareTsukkomiView.as_view()), # 获取微博权限验证页
    url(r'^token/$', views.GetAccessTokenView.as_view()), # 获取微博token 并分享
    url(r'^selectcollect/$', views.SelectCollectTsukkomi.as_view()), # 查询已经收藏的
]
