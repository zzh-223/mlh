from django.conf.urls import url
from . import views

urlpatterns = [
    url(r"^categories/$",views.CategoryView.as_view()),  # 查询栏目类的路由
    url(r"follows/$",views.FollowedView.as_view()), # 用户关注的路由　　提供了get请求　和　post请求
    url(r"news/(?P<pk>\d+)/author_news/$", views.AuthorNewsView.as_view()),  # 用户返回新闻作者发布的所有新闻
    url(r"news/(?P<pk>\d+)/comments/$",views.CommentView.as_view()),
    url(r"news/(?P<pk>\d+)/$", views.NewsDetailView.as_view()),  # 查询新闻详情的路由
    url(r"news/$",views.NewsListView.as_view()),  # 新闻列表的内容

]