from rest_framework.pagination import PageNumberPagination

class StandardResultsSetPagination(PageNumberPagination):
    """定义全局分页的类　
    继承PageNumberPagination
    好处:可以由前端指定每页的数量
    """
    # 默认每页的数量
    page_size = 12
    # 可以指定没有的数量page_size = 数量
    page_size_query_param = 'page_size'
    # 限定每页最大的数量
    max_page_size = 20