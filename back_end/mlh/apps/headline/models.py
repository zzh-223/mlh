from django.db import models
from users.models import User

# Create your models here.

class Category(models.Model):
    """专题栏"""

    # 分类的名字
    name = models.CharField(max_length=50,verbose_name="专题栏分类")
    # 专栏网址
    category_url = models.CharField(max_length=100,default="",verbose_name="专栏网址!")
    # 专栏的简介
    category_digest = models.CharField(max_length=300,default="",verbose_name="专栏简介")
    # 排序
    order_by = models.IntegerField(null=True,verbose_name="排序")
    # 是否被删除
    is_delete = models.BooleanField(default=False,verbose_name="是否被删除")

    class Meta:
        db_table = "tb_category"
        verbose_name = "栏目类表"
        verbose_name_plural = '栏目类表'

    def __str__(self):
        return self.name


class News(models.Model):
    """新闻类"""
    # 新闻标题
    title = models.CharField(max_length=300,verbose_name="标题")
    # 新闻作者
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name="user_all_new",verbose_name="作者",null=True)
    # 所属分类
    category = models.ForeignKey(Category,on_delete=models.CASCADE,related_name="category_all_new",verbose_name="作者",null=True)
    # 新闻摘要
    digest = models.CharField(max_length=500,verbose_name="摘要")
    # 新闻内容
    content = models.CharField(max_length=1000,verbose_name="新闻内容")
    # 新闻图片的url
    index_image_url = models.CharField(max_length=200, default='', null=True, blank=True, verbose_name='默认图片')
    # 发布的时间
    create_time = models.DateTimeField(auto_now_add=True,verbose_name="新闻发布的时间")
    # 浏览时间
    Browsing_time = models.CharField(max_length=100,default="",verbose_name="浏览时间")
    # 新闻点击量
    clicks = models.IntegerField(default=0,verbose_name="点击量")
    # 是否被删除
    is_delete = models.BooleanField(default=False,verbose_name="逻辑删除")

    class Meta:
        db_table = 'info_news'
        verbose_name = '行政区划'
        verbose_name_plural = '行政区划'

    def __str__(self):
        return self.title


class Comment(models.Model):
    """评论表"""
    # 评论的用户
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name="user_comment",verbose_name="评论的用户")
    # 评论的新闻
    news = models.ForeignKey(News,on_delete=models.CASCADE,verbose_name="评论的新闻")
    # 评论的父评论
    parent = models.ForeignKey('self',default="",on_delete=models.CASCADE,null=True, blank=True,verbose_name="评论的父评论")
    # 评论的内容
    content = models.CharField(max_length=300,verbose_name="评论的内容")
    # 每条评论的数量
    count = models.IntegerField(verbose_name="评论的数量",default=0)
    # 评论创建的时间
    create_time = models.DateTimeField(auto_now_add=True,verbose_name="评论的时间")

    class Meta:
        db_table = "headline_tb_comment"
        verbose_name = "栏目类表"
        verbose_name_plural = '栏目类表'


class Followed(models.Model):
    """关注表新闻表"""
    user = models.ForeignKey(User,on_delete=models.CASCADE,verbose_name="用户ID")
    news = models.ForeignKey(News,on_delete=models.CASCADE,verbose_name="新闻的ID")

    class Meta:
        db_table = "tb_followed"
        verbose_name = "关注表"
        verbose_name_plural = '关注表'
