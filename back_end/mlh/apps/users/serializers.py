import re

from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from ansewr.models import Respond, Question
from headline.models import News
from .models import User


class CreateUserSerializer(serializers.ModelSerializer):
    """创建用户的序列化器"""
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    token = serializers.CharField(label='JWT token', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'sms_code', 'mobile', 'allow', 'token')
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 6,
                'max_length': 16,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    def validate_mobile(self, value):
        """验证手机号"""
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def validate_allow(self, value):
        """检验用户是否同意协议"""
        if value != 'true':
            raise serializers.ValidationError('请同意用户协议')
        return value

    def validate(self, data):
        # 判断短信验证码
        redis_conn = get_redis_connection('verify_codes')
        mobile = data['mobile']
        real_sms_code = redis_conn.get('sms_%s' % mobile)
        if real_sms_code is None:
            raise serializers.ValidationError('无效的短信验证码')
        if data['sms_code'] != real_sms_code.decode():
            raise serializers.ValidationError('短信验证码错误')

        return data

    def create(self, validated_data):
        """重写保存方法，增加密码加密"""

        # 移除数据库模型类中不存在的属性
        del validated_data['sms_code']
        del validated_data['allow']

        # user = User.objects.create(username=xxx, password=xx)
        # user = User.objects.create(**validated_data)

        user = super().create(validated_data)

        user.set_password(validated_data['password'])
        user.save()

        # 签发jwt token
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        user.token = token

        return user


class SMSCodeSerializer(serializers.Serializer):
    def validate(self, attrs):
        redis_conn = get_redis_connection('verify_codes')
        mobile = self.context['view'].kwargs['mobile']

        send_flag = redis_conn.get("send_flag_%s" % mobile)
        if send_flag:
            raise serializers.ValidationError('请求次数过于频繁')

        return attrs


class PersonTitleSerializer(serializers.ModelSerializer):
    """
    用户详细信息序列化器
    """
    username = serializers.CharField(read_only=True)
    city = serializers.CharField(read_only=True)
    college = serializers.CharField(read_only=True)
    company = serializers.CharField(read_only=True)
    own_web = serializers.CharField(read_only=True)
    intro = serializers.CharField(read_only=True)
    class Meta:
        model = User
        fields = ('id', 'username', 'city', 'college', 'company', 'own_web', 'intro')

    def create(self, validated_data):
        """
        保存
        """
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class PersonDetailSerializer(serializers.ModelSerializer):
    """
    用户设置序列化器
    """
    class Meta:
        model = User
        fields = ('id', 'username', 'own_web', 'email', 'mobile', 'password')

    # def create(self, validated_data):
    #     """
    #     保存
    #     """
    #     validated_data['user'] = self.context['request'].user
    #     return super().create(validated_data)


class UserBrowsingHistorySerializer(serializers.Serializer):
    news_id = serializers.IntegerField(label="新闻id", min_value=1)
    # title = serializers.CharField(label="新闻标题")

    def validate_news_id(self, value):
        """
        检验news_id是否存在
        """
        try:
            News.objects.get(id=value)
        except News.DoesNotExist:
            raise serializers.ValidationError('该新闻不存在')
        return value

    def create(self, validated_data):
        # news_id
        news_id = validated_data['news_id']

        # user_id
        user = self.context['request'].user

        # redis  [6, 1,2,3,4,5]
        redis_conn = get_redis_connection('history')
        pl = redis_conn.pipeline()

        redis_key = 'history_%s' % user.id
        # 去重
        pl.lrem(redis_key, 0, news_id)

        # 保存 增加
        pl.lpush(redis_key, news_id)

        # 截断
        pl.ltrim(redis_key, 0, 4)

        pl.execute()

        return validated_data


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ("id", 'title')


class MyAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Respond
        fields = ('question_id', 'content', 'create_time')


class MyQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('id', 'title', 'create_time', 'useful')


