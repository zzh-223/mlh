from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class User(AbstractUser):
    """用户模型类"""
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    img_url = models.CharField(max_length=30, null=True, blank=True, default='', verbose_name='头像')
    city = models.CharField(max_length=32, null=True, blank=True, default='', verbose_name='现居城市')
    college = models.CharField(max_length=32, null=True, blank=True, default='', verbose_name='毕业院校')
    company = models.CharField(max_length=32, null=True, blank=True, default='', verbose_name='公司')
    own_web = models.CharField(max_length=32, null=True, blank=True, default='', verbose_name='个人网址')
    intro = models.CharField(max_length=64, null=True, blank=True, default='', verbose_name='个人简介')
    sex = models.BooleanField(default=0, verbose_name='性别')
    address = models.CharField(max_length=64, null=True, blank=True, default='', verbose_name='通讯地址')
    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name




