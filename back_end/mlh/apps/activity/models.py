from django.db import models

# Create your models here.
from users.models import User


class Activity(models.Model):
    STATE_CHOICE = ((0,'正在报名'),(1,'报名截止'))


    title = models.CharField(max_length=50, verbose_name='活动主题')
    address = models.CharField(max_length=50, verbose_name='活动举办地址')
    city = models.CharField(max_length=10,verbose_name='城市')
    organizer = models.CharField(max_length=50, verbose_name='主办方')
    content = models.TextField(verbose_name='议题简介')
    introduce = models.TextField(verbose_name='大会介绍')
    cover = models.ImageField(upload_to='activity', verbose_name='封面图片')
    link_url = models.CharField(max_length=100, verbose_name='活动举办地址')
    # join_people = models.IntegerField(default=0, verbose_name='活动举办地址')
    # share_method = models.SmallIntegerField(choices=SHARE_CHOICES,default=0, verbose_name='活动举办地址')
    state = models.SmallIntegerField(choices=STATE_CHOICE, verbose_name='报名状态')
    start_time = models.DateTimeField(verbose_name='活动开始时间')
    end_time = models.DateTimeField(verbose_name='活动结束时间')
    deadline = models.DateTimeField(verbose_name='报名截止时间')
    create_time = models.DateTimeField(verbose_name='创建活动时间')
    update_time = models.DateTimeField(verbose_name='更新活动时间')
    is_delete = models.BooleanField(verbose_name='活动是否结束')



    class Meta:
        db_table = 'tb_activity'
        verbose_name = '活动'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


class Activity_attend(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, verbose_name='活动')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户')

    class Meta:
        db_table = 'tb_activity_attend'
        verbose_name = '报名活动'
        verbose_name_plural = verbose_name






