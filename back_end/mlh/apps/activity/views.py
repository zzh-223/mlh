from django.shortcuts import render

# Create your views here.
from rest_framework.generics import  CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ReadOnlyModelViewSet

from .models import Activity
from .serializers import ActivityListSerializer, ActivityDetailSerializer


class ActivityViewSet(ReadOnlyModelViewSet):
    queryset = Activity.objects.filter(is_delete=False).order_by('-create_time')
    def get_serializer_class(self):
        if self.action == 'list':
            return ActivityListSerializer
        else:
            return ActivityDetailSerializer


