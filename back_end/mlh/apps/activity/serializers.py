import time
from rest_framework import serializers

from activity.models import Activity, Activity_attend


class ActivityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('id', 'cover', 'title', 'start_time', 'city')


class ActivityDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('id', 'title', 'cover', 'start_time', 'end_time', 'address', 'organizer', 'deadline', 'introduce', 'content', 'link_url', 'state')

    def validate_state(self,value):
        user = self.context['request'].user
        if user:
            if Activity.id in Activity_attend.activity:
                value = 1
                Activity.state = value
                Activity.save()
                return value
            return value
        return value
