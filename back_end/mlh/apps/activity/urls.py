from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from . import views


urlpatterns = [
]

router = DefaultRouter()
router.register('activities', views.ActivityViewSet, base_name='activities')

urlpatterns += router.urls
