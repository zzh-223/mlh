from django.utils import timezone
from rest_framework import serializers

from ansewr.models import Question, Label
from users.models import User


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('id', 'title', 'click', 'create_time', 'user_id', 'useful', 'category','content')


class SubmitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('id','title','user', 'category','content','create_time')
        extra_kwargs = {
            "id":{"read_only":True},
            "create_time":{"read_only":True}
        }

    def create(self, validated_data):
        title = validated_data['title']
        category = validated_data['category']
        content = validated_data['content']
        user = validated_data.get("user",None)
        # 获取用户对象  user
        # user = self.context['request'].user
        return super().create(validated_data)
        # q =Question.objects.create(
        #     user=user,
        #     title=title,
        #     category=category,
        #     content=content,
        # )
        # q.save()


class DetailSerializer(serializers.ModelSerializer):
    """详情"""


    class Meta:
        model = Question
        fields = ('id', 'title', 'create_time', 'user_id', 'content', 'category')
