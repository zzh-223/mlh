from django.shortcuts import render

from rest_framework.permissions import IsAdminUser
# Create your views here.
from rest_framework.filters import OrderingFilter
from rest_framework.generics import GenericAPIView, ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ansewr.models import Question
from ansewr.serializers import QuestionSerializer, SubmitSerializer, DetailSerializer


class QuestionView(ListAPIView):

    queryset = Question.objects.all().order_by("-create_time")
    # print(queryset)
    serializer_class = QuestionSerializer
    pagination_class = None



class Submit(CreateAPIView):
    """发布问题"""
    # 配置权限  只允许登录用户访问
    # permission_classes = [IsAdminUser]
    # 指定序列化起
    serializer_class = SubmitSerializer


class DetailView(RetrieveAPIView):

    serializer_class = DetailSerializer

    def get_queryset(self):
        question = Question.objects.filter(id=self.kwargs['pk'])
        return question
