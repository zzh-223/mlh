from django.db import models

# Create your models here.
from users.models import User


class Label(models.Model):
    """标签表"""
    id = models.IntegerField(primary_key=True, unique=True, verbose_name='id')
    name = models.CharField(max_length=20, verbose_name='标签')

    class Meta:
        # abstract = True
        db_table = 'tb_label'
        verbose_name = '标签表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Question(models.Model):
    """问表"""
    title = models.CharField(max_length=20, verbose_name='标题')
    content = models.TextField(null=True, blank=True, verbose_name='问题内容')
    useful = models.BooleanField(default=False,verbose_name='有用')
    category = models.ForeignKey(Label, on_delete=models.PROTECT, verbose_name='标签')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='作者')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新时间")
    click = models.IntegerField(default=0,verbose_name='点击量')

    class Meta:
        db_table = 'tb_question'
        verbose_name = '问表 '
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


class Respond(models.Model):
    """答表"""
    id = models.IntegerField(primary_key=True, unique=True, verbose_name='id')
    parent_id = models.ForeignKey('self', verbose_name='父类id')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新时间")
    user_id = models.ForeignKey(User, verbose_name='用户id')
    content = models.TextField(null=True, blank=True, verbose_name='回答内容')
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE, verbose_name='问题id')

    class Meta:
        db_table = 'tb_respond'
        verbose_name = '答表 '
        verbose_name_plural = verbose_name


class Attention(models.Model):
    """关注表"""
    id = models.IntegerField(primary_key=True, unique=True, verbose_name='id')
    label_id = models.ForeignKey(Label, on_delete=models.CASCADE, verbose_name='标签分类')
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户名')
    count = models.IntegerField(null=True, verbose_name='关注人数')

    class Meta:
        db_table = 'tb_attention'
        verbose_name = '关注 '
        verbose_name_plural = verbose_name


