from django.contrib import admin

# Register your models here.
from ansewr import models

admin.site.register(models.Label)
admin.site.register(models.Question)
admin.site.register(models.Respond)
admin.site.register(models.Attention)