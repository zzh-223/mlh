from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^question/$', views.QuestionView.as_view()),
    url(r'^detail/(?P<pk>\d+)$', views.DetailView.as_view()),
    url(r'^submit/$', views.Submit.as_view()),
]
