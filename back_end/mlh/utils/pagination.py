# 自定义全局分页配置
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    """自定义分页类"""
    page_size = 2  # 当前端未定义每页显示多少条时，按此大小显示
    page_size_query_param = 'page_size'  # 前端传送每页显示多少的变量名
    max_page_size = 20
