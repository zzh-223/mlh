var vm = new Vue({
    el: '#app',
    data: {
        host: host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        category_id: '', // 当前模块类别
        page: 1, // 当前页数
        page_size: 2, // 每页数量
        ordering: '-create_time', // 排序
        count: 5,  // 总数量
        tsukkomis: [], // 数据
        is_collect:false,
    },
    computed: {
        total_page: function(){  // 总页数
            return Math.ceil(this.count/this.page_size);
        },
        next: function(){  // 下一页
            if (this.page >= this.total_page) {
                return 0;
            } else {
                return this.page + 1;
            }
        },
        previous: function(){  // 上一页
            if (this.page <= 0 ) {
                return 0;
            } else {
                return this.page - 1;
            }
        },
        page_nums: function(){  // 页码
            // 分页页数显示计算
            // 1.如果总页数<=5
            // 2.如果当前页是前3页
            // 3.如果当前页是后3页,
            // 4.既不是前3页，也不是后3页
            var nums = [];
            if (this.total_page <= 5) {
                for (var i=1; i<=this.total_page; i++){
                    nums.push(i);
                }
            } else if (this.page <= 3) {
                nums = [1, 2, 3, 4, 5];
            } else if (this.total_page - this.page <= 2) {
                for (var i=this.total_page; i>this.total_page-5; i--) {
                    nums.push(i);
                }
            } else {
                for (var i=this.page-2; i<this.page+3; i++){
                    nums.push(i);
                }
            }
            return nums;
        }
    },
    // 进入页面时自动加载的第一页数据
    mounted: function(){
        if(this.token){
            axios.get(this.host+'/tsukkomi/', {
            params: {page: this.page, page_size: this.page_size, ordering: this.ordering},
            headers: {'Authorization': 'JWT ' + this.token},
            responseType:'json'
            })
            .then(response => {
            this.count = response.data.count;
            this.tsukkomis = response.data.results;
                for (var i = 0; i < this.tsukkomis.length; i++) {
                    this.tsukkomis[i].url ='/spit-detail.html?id='+this.tsukkomis[i].id;
                }
            })
            .catch(error => {
                console.log(error.response.data)
        });
        }else(
            axios.get(this.host+'/tsukkomi/', {
            params: {page: this.page, page_size: this.page_size, ordering: this.ordering},
            responseType:'json'
            })
            .then(response => {
                this.count = response.data.count;
                this.tsukkomis = response.data.results;
                    for (var i = 0; i < this.tsukkomis.length; i++) {
                        this.tsukkomis[i].url ='/spit-detail.html?id='+this.tsukkomis[i].id;
                    }
            })
            .catch(error => {
                console.log(error.response.data)
            })
        )
},
    methods: {
        // 请求吐槽数据
        get_tsukkomi: function () {
            if(this.token){
                axios.get(this.host + '/tsukkomi/', {
                params: {page: this.page, page_size: this.page_size, ordering: this.ordering},
                headers: {'Authorization': 'JWT ' + this.token},
                responseType: 'json'
            })
            .then(response => {
                this.tsukkomis = response.data.results;
                for (var i = 0; i < this.tsukkomis.length; i++) {
                    this.tsukkomis[i].url ='/spit-detail.html?id='+this.tsukkomis[i].id;
                }
            })
            .catch(error => {
                console.log(error.response.data);
            })
            }else(
                axios.get(this.host + '/tsukkomi/', {
                params: {page: this.page, page_size: this.page_size, ordering: this.ordering},
                responseType: 'json'
            })
            .then(response => {
                this.tsukkomis = response.data.results;
                for (var i = 0; i < this.tsukkomis.length; i++) {
                    this.tsukkomis[i].url ='/spit-detail.html?id='+this.tsukkomis[i].id;
                }
            })
            .catch(error => {
                console.log(error.response.data);
            })
            )
        },
        // 点击页数
        on_page: function (num) {
            if (num != this.page) {
                this.page = num;
                this.get_tsukkomi();
            }
        },
        // 收藏吐槽
        collect_tsukkomi: function (event) {
            if(this.token){
            axios.post(this.host +"/collect/tsukkomi/",{
            tsukkomi:event.currentTarget.id,
            }, {
            headers: {
                    'Authorization': 'JWT ' + this.token
                },
            responseType: 'json',

            })
            .then(response => {
            for (var i = 0; i < this.tsukkomis.length; i++) {
                if(this.tsukkomis[i].id==response.data.tsukkomi){
                    this.tsukkomis[i]["is_collect"]=true;
                    alert("收藏成功");
                    this.tsukkomi=null;
                    return}
                }
            alert("收藏的内容未找到")
                                })
            .catch(error => {
            console.log(error.response.data)
                    });
            }else(window.open('person-loginsign.html'))

            },

        // 点赞吐槽
        like_tsukkomi:function (event) {
            if(this.token){axios.post(this.host +"/like/tsukkomi/",{
                tsukkomi:event.currentTarget.id,

                }, {
                    headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json',
                    withCredentials: true
                })
            .then(response => {
                for (var i = 0; i < this.tsukkomis.length; i++) {
                    if(this.tsukkomis[i].id==response.data.tsukkomi){
                        this.tsukkomis[i]["like_count"]+=1;
                        alert("点赞成功");
                        this.tsukkomi=null;
                        return}
                    }
            })
            .catch(error => {
                console.log(error.response.data)
                });

            }else(window.open('person-loginsign.html'))

        },
        // 分享
        share_tsukkomi:function (event) {
            axios.get(this.host +"/share/tsukkomi?tsukkomi_id="+event.currentTarget.id,{
                }, {
                    responseType: 'json',
                    withCredentials: true
                })
            .then(response => {
                window.open(response.data.wb_url);
            })
            .catch(error => {
                alert(error.response.data.message);
                console.log(error.response.data)
                });
        },
        // 退出登录
        log_out:function () {
            sessionStorage.clear();
            localStorage.clear();
        }

    }
});