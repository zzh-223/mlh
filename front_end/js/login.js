var vm = new Vue({
        el: '#apps',
        data: {
            host: host,
            error_mobile: false,
            error_pwd: false,
            username: '',
            password: '',
            remember: false
        },
        methods: {
                // 获取url路径参数
                get_query_string: function (name) {
                    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null) {
                        return decodeURI(r[2]);
                    }
                    return null;
                },
            // 检查数据
            check_username: function () {
                if (!this.username) {
                    this.error_mobile = true;
                } else {
                    this.error_username = false;
                }
            },
            check_pwd: function () {
                if (!this.password) {
                    this.error_pwd = true;
                } else {
                    this.error_pwd = false;
                }
            },
            on_submit: function () {
                this.check_username();
                this.check_pwd();

                if (this.error_username == false && this.error_pwd == false
                ) {
                    axios.post(this.host + '/authorizations/', {
                        password: this.password,
                        username: this.username,
                        remember: this.remember.toString()
                    }, {
                        responseType: 'json',
                        withCredentials: true
                    })
                        .then(response => {
                            // 使用浏览器本地存储保存token
                            if (this.remember) {
                                // 记住登录
                                sessionStorage.clear();
                                localStorage.token = response.data.token;
                                localStorage.user_id = response.data.user_id;
                                localStorage.username = response.data.username;
                            } else {
                                // 未记住登录
                                localStorage.clear();
                                sessionStorage.token = response.data.token;
                                sessionStorage.user_id = response.data.user_id;
                                sessionStorage.username = response.data.username;
                            }
                            var return_url = this.get_query_string('next');
                            if (!return_url) {
                                return_url = '/index.html';
                            }
                            location.href = return_url;

                        })
                        .catch(error => {
                            alert('error')
                        })
                }
            }
        }
    }
        )