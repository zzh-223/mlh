
var vm = new Vue({
    el: '#app',
    data: {
        host,
        delimiters: ['[[', ']]'],
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        page: 1, // 当前页数
        page_size: 3, // 每页数量
        ordering: '-create_time', // 排序
        count: 1,  // 总数量
        tsukkomi: "", // 吐槽详情
        tsukkomi_id:"",

        comments:"",  // 获取到的评论数据
        message:"",  // 输入的评论消息
        parent:"", // 主评论id
        comment_content:"",//输入的评论数据
    },
    // 分页数据
    computed: {
        total_page: function(){  // 总页数
            return Math.ceil(this.count/this.page_size);
        },
        next: function(){  // 下一页
            if (this.page >= this.total_page) {
                return 0;
            } else {
                return this.page + 1;
            }
        },
        previous: function(){  // 上一页
            if (this.page <= 0 ) {
                return 0;
            } else {
                return this.page - 1;
            }
        },
        page_nums: function(){  // 页码
            // 分页页数显示计算
            // 1.如果总页数<=5
            // 2.如果当前页是前3页
            // 3.如果当前页是后3页,
            // 4.既不是前3页，也不是后3页
            var nums = [];
            if (this.total_page <= 5) {
                for (var i=1; i<=this.total_page; i++){
                    nums.push(i);
                }
            } else if (this.page <= 3) {
                nums = [1, 2, 3, 4, 5];
            } else if (this.total_page - this.page <= 2) {
                for (var i=this.total_page; i>this.total_page-5; i--) {
                    nums.push(i);
                }
            } else {
                for (var i=this.page-2; i<this.page+3; i++){
                    nums.push(i);
                }
            }
            return nums;
        }
    },
        // 获取吐槽详情数据
        mounted: function () {
        // 从路径中提取tsukkomi_id
        var re = /^.*\?id=(\d+)$/;
        this.tsukkomi_id =window.location.href.match(re)[1];
        axios.get(this.host + '/tsukkomi/' + this.tsukkomi_id+"/",
            {
                responseType: 'json'
            })
            .then(response => {
            this.tsukkomi = response.data;
            })
            .catch(error => {
                console.log(error.response.data)
            });
        // 请求第一页评论数据
        axios.get(this.host + '/comment/' + this.tsukkomi_id+"/"+'?page='+this.page +'&page_size='+this.page_size +'&ordering=-create_time',{
            responseType: 'json'
        })
        .then(response => {
            this.comments = response.data.results;
            this.count = response.data.count;
            })
        .catch(error => {
                console.log(error.response.data)
            });
    },
    // 点击评论翻页
    methods: {
        // 请求评论数据
        get_comment: function () {
            axios.get(this.host + '/comment/'+ this.tsukkomi_id+"/", {
                params: {
                    page: this.page,
                    page_size: this.page_size,
                    ordering: this.ordering
                },
                responseType: 'json'
            })
                .then(response => {
                    this.comments = response.data.results;
                })
                .catch(error => {
                    console.log(error.response.data);
                })
        },
        // 点击页数
        on_page: function (num) {
            if (num != this.page) {
                this.page = num;
                this.get_comment();
            }
        },
        show_reply:function (event){
            vm.comments.splice(this.comments.length)
            this.parent = event.currentTarget.id;  // 获取主评论id
            if (event.currentTarget.nextSibling.nextSibling.style.display==="block")
            {event.currentTarget.nextSibling.nextSibling.style.display="none"}else{
                event.currentTarget.nextSibling.nextSibling.style.display="block"
            }
        },
        // 提交子评论
        on_submit:function (event) {
            if (event.currentTarget.style.display==="block")
            {event.currentTarget.style.display="none"}else{
                event.currentTarget.style.display="block"
            }
            if(this.token){axios.post(this.host +"/creation/comment/",{
                message:this.message,
                tsukkomi:this.tsukkomi_id,
                parent:this.parent,

                }, {
                headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                responseType: 'json',

                })
            .then(response => {
                for (var i = 0; i < this.comments.length; i++) {
                    if (this.comments[i].id == response.data.parent) {
                        this.comments[i].son_comments.push(response.data)
                    }
                    this.message =null
                }
            })
            .catch(error => {
                        console.log(error.response.data)
                    });
            }else(window.open('person-loginsign.html'))


        },
        // 提交主评论
        comment_submit:function () {
            if(this.token){axios.post(this.host +"/creation/comment/",{
                message:this.comment_content,
                tsukkomi:this.tsukkomi_id,
                parent:null,

                }, {
                headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                responseType: 'json',
                })
                .then(response => {
                this.comments.splice(0,0,response.data);
                this.comment_content=null;
                window.location.reload()

            })
            .catch(error => {
                console.log(error.response.data)
                    });
            }else(window.open('person-loginsign.html'))

        },
        // 点赞评论
        like_comment:function (event) {
            if(this.token){axios.post(this.host +"/like/comment/",{
                comment:event.currentTarget.id,
                }, {
                    headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json',
                    withCredentials: true
                })
            .then(response => {
                for (var i = 0; i < this.comments.length; i++) {
                    if(this.comments[i].id==response.data["comment"]){
                        this.comments[i]["like_count"]+=1;
                        alert("点赞成功");
                        return}
                    }
            })
            .catch(error => {
                console.log(error.response.data)
                });
            }else(window.open('person-loginsign.html'))
        },

        // 点赞吐槽
        like_tsukkomi:function (event) {
            if(this.token){
                axios.post(this.host +"/like/tsukkomi/",{
                tsukkomi:event.currentTarget.id,

                }, {
                    headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json',
                    withCredentials: true
                })

            .then(response => {
                alert("点赞成功");
            })
            .catch(error => {
                alert(error.response.data.message);
                console.log(error.response.data)
                });
            }else(window.open('person-loginsign.html'))


        },
        log_out:function () {
            sessionStorage.clear();
            localStorage.clear();
        }
    }
});