/**
 * Created by python on 18-11-10.
 */
var vm = new Vue({
    el:"#app_category",
    data:{
        // 用户的变量　判断用户是否登录
        //　表示从本地获取还是从session中获取　
        // 当用户登录时，服务器会把用户的名字，ｉｄ,图片链接返回给浏览器
        // 如果浏览器选择了记住密码则　吧这些信息存储到本地缓存中localStorage  反之存储到session中
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        user_img_url:sessionStorage.img_url || localStorage.img_url,
        token: sessionStorage.token || localStorage.token,
        // 存储的是访问的地址127.0.0.1:8000
        host:host,
        host_q:host_q,
        // 用来存储专题栏的名字
        c_name:"",
        // 用来存储专题来的网址
        c_category_url:"",
        // 用来存储专题栏的简介
        c_digest:"",
    },
    methods: {
        // 请求热门分类的新闻
        addCategory:function () {
            //    发起请求
            axios.post(this.host + '/categories/', {
                    name: this.c_name,
                    category_url: this.c_category_url,
                    category_digest: this.c_digest
                })
                .then(response => {
                    alert("提交成功!");
                    window.location.href = 'http://127.0.0.1:8080/headline-logined.html';
                })
                .catch(error => {
                    alert("提交失败");
                    console.log(error);
                })
        },
    }

})
