const host ="http://127.0.0.1:8000";
const host_q = "http://127.0.0.1:8080";
//将字符串转化成date类数据
Vue.filter('str2date', function (date_str) {
    return new Date(date_str)
});
//将date数据转化成年月日
Vue.filter('date2ymd', function (date) {
    return date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate()
});
//将date数据转化成时分
Vue.filter('date2hm', function (date) {
    return date.getHours() + ':' + date.getMinutes()
});
//将date数据转化成周
const WEEK = ['周日','周一','周二','周三','周四','周五','周六']
Vue.filter('date2w', function (date) {
    return WEEK[date.getDay()]
});

// 获取url路径参数
function get_query_string(name){
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return decodeURI(r[2]);
    }
    return null;
};
// 多久之前计时统计
Vue.filter('fomatTime', function (valueTime) {

  if(valueTime){
    var newData =  Date.parse(new Date());
    var diffTime = Math.abs(newData-valueTime);
    if (diffTime > 7 * 24 * 3600 * 1000) {
      var date = new Date(valueTime);
      var y = date.getFullYear();
      var m = date.getMonth() + 1;
      m = m < 10 ? ('0' + m) : m;
      var d = date.getDate();
      d = d < 10 ? ('0' + d) : d;
      var h = date.getHours();
      h = h < 10 ? ('0' + h) : h;
      var minute = date.getMinutes();
      var second = date.getSeconds();
      minute = minute < 10 ? ('1' + minute) : minute;
      second = second < 10 ? ('0' + second) : second;
      return  m + '-' + d+' '+h+':'+minute;

    } else if (diffTime < 7 * 24 * 3600 * 1000 && diffTime > 24 * 3600 * 1000) {
      // //注释("一周之内");

      // var time = newData - diffTime;
      var dayNum = Math.floor(diffTime / (24 * 60 * 60 * 1000));
      return dayNum + "天前";

    } else if (diffTime < 24 * 3600 * 1000 && diffTime > 3600 * 1000) {
      // //注释("一天之内");
      // var time = newData - diffTime;
      var dayNum = Math.floor(diffTime / (60 * 60 * 1000));
      return dayNum + "小时前";

    } else if (diffTime < 3600 * 1000 && diffTime > 0) {
      // //注释("一小时之内");
      // var time = newData - diffTime;
      var dayNum = Math.floor(diffTime / (60 * 1000));
      return dayNum + "分钟前";

    }
  }
});




