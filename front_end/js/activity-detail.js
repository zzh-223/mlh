let app = new Vue({
    el: '#app',
    data:{
    //    页面需要使用的数据
        activity : null,
        now_time_ms : 0,
        last_time : 0,
        deadline_ms : 0,
        token: sessionStorage.token || localStorage.token,
        // state: 0
    },
    computed:{
    //    需要通过计算得到的数据
    },
    mounted:function () {
        // 获取页面ｉｄ
        id = get_query_string('id')

        axios.get(host+'activities/' + id + '/').
        then(response => {
            // console.info(response.data)
            this.activity = response.data
            let now_time = response.headers.date
            now_time = new Date(now_time)
            this.countdown(new Date(response.data.deadline).getTime(),now_time.getTime())
        }).
        catch(error => {
            // alert('error')
        })
    },
    methods:{
        //倒计时
        countdown: function (deadline_ms,now_time_ms) {
            this.now_time_ms = now_time_ms
            this.deadline_ms = deadline_ms
            setInterval(()=>{
                this.now_time_ms += 1000
                this.last_time = this.deadline_ms - this.now_time_ms
            },1000)
        },
    },
    filters:{
        ms2dhms:function (ms) {
            var day = Math.floor(ms /1000/60/60/24)
            var hour = Math.floor(ms /1000/60/60%24)
            var minute = Math.floor(ms /1000/60%60)
            var second = Math.floor(ms /1000%60)
            return day+'天'+hour+'时'+minute+'分'+ second+'秒'
        }
    }
});