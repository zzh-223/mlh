/**
 * Created by python on 18-11-12.
 */
var vm = new Vue({
    el:"#submit_app",
    data:{
                // 用户的变量　判断用户是否登录
        //　表示从本地获取还是从session中获取　
        // 当用户登录时，服务器会把用户的名字，ｉｄ,图片链接返回给浏览器
        // 如果浏览器选择了记住密码则　吧这些信息存储到本地缓存中localStorage  反之存储到session中
        q_username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        user_img_url:sessionStorage.img_url || localStorage.img_url,
        token: sessionStorage.token || localStorage.token,
        // 存储的是访问的地址127.0.0.1:8000
        host:host,
        host_q:host_q,
    //    保存分类列表
        category_list:[],
        // 存储头条的标题
        headline_title:"",
    //    存储头条分类的id
        headline_cate:[],
    //    存储头条的摘要
        headline_desc:"",

        //存储选中分类的id
        selectId:'',

    },
    mounted:function () {
        CKEDITOR.replace('editor1');
        // 请求分类
        axios.get(this.host+'/categories/',{
            responseType:'json'
        })
        .then(response =>{
            this.category_list = response.data;
            // console.log(this.category_list)
        })
        .catch(error => {
            console.log(error)
        })
          //
    },
    methods:{
    //    提交的js
        submit_headline:function () {
        //    获取表单的标题
        //    获取表单分类的id
        //    获取标签也就是摘要
        //    获取文本域内容
            var content_edict = CKEDITOR.instances.editor1.getData();
            axios.post(this.host+'/news/',{
                user:2,
                title:this.headline_title,
                category:this.selectId,
                digest:this.headline_desc,
                content:content_edict,
            })
            .then(response =>{
                window.location.href='127.0.0.1:8080/headline-logined.html'
            })
            .catch(error => {
                console.log(error)
            })
        }
    }
})