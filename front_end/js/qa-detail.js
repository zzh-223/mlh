/**
 * Created by python on 18-11-10.
 */
/**
 * Created by python on 18-11-9.
 */
let vm = new Vue({
     // 页面中需要使用到的数据，键值对
    el:"#app",
    data:{
        question: '',
        category:""
    },
     // 一加载就需要做的，直接是代码
    mounted: function(){
        id = get_query_string('id')
        // 获取首页数据
        axios.get(host+'/detail/'+id)
            .then(response => {
                 // 加载用户数据
                this.question = response.data

            })
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
         // 获取url路径参数
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
    }
})