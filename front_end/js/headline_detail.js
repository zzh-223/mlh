/**
 * Created by python on 18-11-10.
 */
var vm = new Vue({
    el:"#app_news_detail",
    data:{
        // 用户的变量　判断用户是否登录
        //　表示从本地获取还是从session中获取　
        // 当用户登录时，服务器会把用户的名字，ｉｄ,图片链接返回给浏览器
        // 如果浏览器选择了记住密码则　吧这些信息存储到本地缓存中localStorage  反之存储到session中
        q_username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        user_img_url:sessionStorage.img_url || localStorage.img_url,
        token: sessionStorage.token || localStorage.token,
        // 存储的是访问的地址127.0.0.1:8000
        host:host,
        host_q:host_q,
        // 存储的是分类的内容
        categories:[],
        // 存储新闻的内容
        news_detail:[],
        author_news:[],
        news_comment:[],
        // 存储用户关注的新闻
        user_follow_news:[],
        articles:[],
        // 评论的内容
        content:[],
        // 用来记录回复框隐藏还是显示  默认为false
        is_hei:[],
        // 记录子评论的内容
        sub_content:[],
        // 存储热门吐槽
        tucaos:[]
    },
    computed:{
        comment_count:function () {
            if (this.news_comment){
                return this.news_comment.length
            }
        }
    },
    mounted:function () {
        // 获取url里面的id
        var pk = this.get_query_string('id');
        // alert(pk)
        // 根据参数发起axios请求
        axios.get(this.host+'/news/'+pk,{
            responseType:'json'
        })
        .then(response => {
            // console.log(response.data);
            this.news_detail = response.data
        })
        .catch(error => {
            console.log(error)
        });
        // 请求所有的评论
        axios.get(this.host+'/news/'+pk+'/comments/',{
            responseType:'json'
        })
        .then(response => {
            this.news_comment = response.data;
            for(var i=0; i<this.news_comment.length; i++){
                Vue.set(this.news_comment[i],"is_hei",false);
                // alert(this.news_comment[i])
            }
        })
        .catch(error => {
            console.log(error)
        });
        // 请求当前新闻作者的所有新闻
        axios.get(this.host+'/news/'+pk+'/author_news/',{
            responseType:'json'
        })
        .then(response =>{
            this.author_news = response.data;
        })
        .catch(error => {
            console.log(error)
        });
    //    获取热门吐槽
        axios.get(this.host + "/tsukkomi/",{
            responseType:'json'
        })
        .then(response =>{
            this.tucaos=response.data;
            console.log(this.tucaos);
        })
        .catch(error => {
            console.log(error)
        })
    },
    methods:{
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        add_focus:function () {
            if (this.q_username){
                // 获取当前新闻的id
                // 获取当前的用户的id
                // 发起axios请求
                axios.post(this.host+'/follow/',{
                    news_id:this.get_query_string('id'),
                    user_id:this.user_id
                })
                .then(response => {
                    console.log(response.data)
                })
            }
        },
        addComment:function (parent_index,index) {
          // alert(parent_index);
          // alert(this.content);
          // alert(index);
        　
          //   判断当前父类id是否有值
          // if (parent_index){
          //     params={
          //       news:this.get_query_string('id'),
          //       content:this.content,
          //       parent:parent_index,
          //       user:1
          //     }
          // }else {
          //       params={
          //       news:this.get_query_string('id'),
          //       content:this.content,
          //       user:1
          //     }
          // }
         // if (parent_index) {
         //     var pk = parseInt(this.get_query_string('id'));
         //     alert(pk);
         //     alert(this.sub_content);
         //     //  获取父评论的id
         //     axios.post(this.host + '/news/' + pk + '/comments/', {
         //             news:pk,
         //             content:this.sub_content,
         //             // parent:parent_index,
         //             user:1,
         //         }
         //     )
         //     .then(response => {
         //         // 请求成功则添加到评论列表里面
         //         this.news_comment.splice(0, 0, response.data);
         //         // this.content = null
         //     })
         // }
     // .catch(error => {
     //          console.log(error)
     //      })
     //    }else {
          var pk = this.get_query_string('id');

          //  获取父评论的id
          axios.post(this.host+'/news/'+pk+'/comments/', {
                news:parseInt(this.get_query_string('id')),
                content:this.content,
                parent:parent_index,
                user:1,
              }
            )
          .then(response => {
            // 请求成功则添加到评论列表里面
              this.news_comment.splice(0,0,response.data);
              this.content = null;
              i = index+1;
              Vue.set(this.news_comment[i],"is_hei",false);
          })
          .catch(error => {
              console.log(error)
          });
        },
        changeHie:function (index) {
        //    判断当前的是隐藏还是显示
        //     获取当前评论对象是隐藏还是显示状态
            var dis = this.news_comment[index].is_hei;
            alert(dis);
            if (dis){
                Vue.set(this.news_comment[index],"is_hei",false);
            }else{
                for(var i=0; i<this.news_comment.length; i++){
                    Vue.set(this.news_comment[i],"is_hei",false);
                // alert(this.news_comment[i])
                    }
                Vue.set(this.news_comment[index],"is_hei",true);
            }
        }
    }

})