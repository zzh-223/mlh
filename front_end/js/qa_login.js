/**
 * Created by python on 18-11-9.
 */
let app = new Vue({
     // 页面中需要使用到的数据，键值对
    el:"#app",
    data:{
        question:[],

    },
     // 一加载就需要做的，直接是代码
    mounted: function(){
        // 获取首页数据
        axios.get(host+'/question/')
            .then(response => {
                 // 加载用户数据
                this.question = response.data

            })
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})