/**
 * Created by python on 18-11-14.
 */
/**
 * Created by python on 18-11-12.
 */
var vm = new Vue({
    el:"#apps",
    data:{
        host,
        token: sessionStorage.token || localStorage.token,
         // 页面中需要使用到的数据，键值对
        titles: {}
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
         axios.get(this.host + '/myanswers/', {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json'
            })
            .then(response => {
                this.titles = response.data;
            })
             .catch(error => {
           alert('error')

        })
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})