/**
 * Created by python on 18-11-12.
 */
var vm = new Vue({
    el:"#submit_app",
    data:{
                // 用户的变量　判断用户是否登录
        //　表示从本地获取还是从session中获取　
        // 当用户登录时，服务器会把用户的名字，ｉｄ,图片链接返回给浏览器
        // 如果浏览器选择了记住密码则　吧这些信息存储到本地缓存中localStorage  反之存储到session中
        q_username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        user_img_url:sessionStorage.img_url || localStorage.img_url,
        token: sessionStorage.token || localStorage.token,
        // 存储的是访问的地址127.0.0.1:8000
        host:host,
    //存储的标题
        title:"",
    //    存储的是标签
        category:"",
    //    存储的问题的内容
        content:""

    },
    mounted:function () {
        CKEDITOR.replace('editor1');
        // // 请求分类
        // axios.get(this.host+'/categories/',{
        //     responseType:'json'
        // })
        // .then(response =>{
        //     this.category_list = response.data;
        //     alert(response.data);
        //     console.log(this.category_list)
        // })
        // .catch(error => {
        //     console.log(error)
        // })
        //   //
    },
    methods:{
    //    提交的js
        submit_question:function () {
        //    获取表单的标题
        //    获取表单分类的id
        //    获取标签也就是摘要
        //    获取文本域内容
            var content_edict = CKEDITOR.instances.editor1.getData();
            axios.post(this.host+'/submit/',{
                title:this.title,
                category:1,
                content:content_edict,
                user:1
            })
            .then(response =>{
                alert("提交成功！");
                window.location.href='http://127.0.0.1:8080/qa-login.html'
            })
            .catch(error => {
                console.log(error)
            })
        }
    }
})


