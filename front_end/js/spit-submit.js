var vm = new Vue({
    el: '#app',
    data: {
        host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
    },
    methods:{
        log_out:function () {
            sessionStorage.clear();
            localStorage.clear();
        }
    }

//     methods: {
//         // 提交吐槽文本
//         content_submit: function () {
//             alert(this.input_content);
//             axios.post(this.host + "/creation_tsukkomi/", {
//                 content: this.input_content,
//             }, {
//                 responseType: 'json'
//             })
//                 .then(response => {
//
//                 })
//                 .catch(error => {
//                     console.log(error.response.data)
//                 });
//
//         }
//     }
});

$(function() {
    $(".content").submit(function () {
        // var content = CKEDITOR.instances['content'].document.getBody().getText(); // 获取所有文本内容不包括链接
        var content = CKEDITOR.instances['content'].getData(); // 获取所有文本内容
        var token =  sessionStorage.token || localStorage.token;
        var params = {
            "content": content,
        };
        if(token){$.ajax({
            url:host+"/creation/tsukkomi/",
            method: "post",
            data: JSON.stringify(params),
             headers: {
            'Authorization': 'JWT ' + token
                    },
            contentType: "application/json",
            success: function (resp) {
                if (resp) {
                    // 刷新当前界面
                    alert("OK");
                    // location.reload();
                } else {
                    alert(resp.errmsg);
                    // location.reload();
                }
            }
        })}else{window.open('person-loginsign.html')}

    })
});




