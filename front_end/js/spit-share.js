var vm = new Vue({
    el: '#app',
    data: {
        host,
        code:""
    },
    mounted: function () {
        // 从路径中提取tsukkomi_id
        var re = /^.*\?(code=\w+)$/;
        this.code =window.location.href.match(re)[1];
        axios.get(this.host + '/token?'+this.code,
            {
                responseType: 'json'
            })
            .then(response => {
                alert(response.data.wbuser_id)

            })
            .catch(error => {
                console.log(error.response.data)
            });
    }
});