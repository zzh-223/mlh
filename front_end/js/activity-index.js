/**
 * Created by python on 18-11-8.
 */
let app = new Vue({
    el: '#app',
    data:{
    //    页面需要使用的数据
        activities : []
    },
    computed:{
    //    需要通过计算得到的数据
    },
    mounted:function () {
    //    一加载就需要做的
        axios.get(host+'activities/').
        then(response => {
            // console.info(response.data)
            this.activities = response.data
        }).
        catch(error => {
            alert('error')
        })
    },
    methods:{
    //
    }
});