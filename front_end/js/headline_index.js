/**
 * Created by python on 18-11-10.
 */
var vm = new Vue({
    el:"#app_category",
    data:{
        // 用户的变量　判断用户是否登录
        //　表示从本地获取还是从session中获取　
        // 当用户登录时，服务器会把用户的名字，ｉｄ,图片链接返回给浏览器
        // 如果浏览器选择了记住密码则　吧这些信息存储到本地缓存中localStorage  反之存储到session中
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        user_img_url:sessionStorage.img_url || localStorage.img_url,
        token: sessionStorage.token || localStorage.token,
        // 存储的是访问的地址127.0.0.1:8000
        host:host,
        host_q:host_q,
        // 存储的是分类的内容
        categories:[],
        // 存储新闻的内容
        news:[],
        // 存储用户关注的新闻
        user_follow_news:[],
        news_follow:[],
        articles:[],
        // 用来表是移除类
        current:-1,
        // 保存当前用户的关注的新闻
        foll:[],

    },
    computed: {

    },
    mounted:function () {
        //    页面加载后执行该函数内的功能
        //　获取热门分类的数据
        axios.get(this.host+'/categories/', {
                responseType:'json'
        })
        .then(response => {
            this.categories = response.data
        })
        .catch(error => {
                console.log(error)
        });
        // 获取所有的新闻列表
        axios.get(this.host+'/news/',{
            responseType:'json'
        })
        .then(response => {
            // alert("呵呵呵呵");
            this.news = response.data;
            for (var i=0;i<this.news.length;i++){
                Vue.set(this.news[i],"is_followed",false)
            }
            // console.log(this.news)
        })
        .catch(error => {
            console.log(error.error)
        });
        if (this.username){
            axios.get(this.host+'/follows/', {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
                withCredentials: true
            })
            .then(response => {
                // this.news_follow = this.news;
                // alert(typeof(this.user_follow_news));
                for (var i=0;i<response.data.length;i++){

                    var news_id=response.data[i].news;

                    for (var j=0;j<this.news.length;j++){
                        // alert(this.news[j].id);
                        // alert(typeof (this.news[j].id));
                        // alert(news_id);
                        // alert(typeof (news_id));
                        if (this.news[j].id==news_id){
                            Vue.set(this.news[j],"is_followed",true)
                        }
                    }
                }
                // 哈哈哈
                // alert(typeof(this.user_follow_news));
                // alert(this.user_follow_news);
                // console.log(this.user_follow_news)
            })
            .catch(error => {
                console.log(error)
            });
        }
    //                 // 缓存指针
    // let _this = this;
    // // 设置一个开关来避免重负请求数据
    // let sw = true;
    // // 此处使用node做了代理
    // axios.get('http://localhost:8000/proxy?url=http://news-at.zhihu.com/api/4/news/latest')
    //     .then(function(response){
    //         // console.log(JSON.parse(response.data).stories);
    //         // 将得到的数据放到vue中的data
    //         _this.articles = JSON.parse(response.data).stories;
    //     })
    //     .catch(function(error){
    //         console.log(error);
    //     });

    // 注册scroll事件并监听
    // window.addEventListener('scroll',function(){
    //     // console.log(document.documentElement.clientHeight+'-----------'+window.innerHeight); // 可视区域高度
    //     // console.log(document.body.scrollTop); // 滚动高度
    //     // console.log(document.body.offsetHeight); // 文档高度
    //     // 判断是否滚动到底部
    //     if(document.body.scrollTop + window.innerHeight >= document.body.offsetHeight) {
    //         // console.log(sw);
    //         // 如果开关打开则加载数据
    //         if (sw == true) {
    //             // 将开关关闭
    //             sw = false;
    //             axios.get('http://localhost:8000/proxy?url=http://news.at.zhihu.com/api/4/news/before/20170608')
    //                 .then(function (response) {
    //                     console.log(JSON.parse(response.data));
    //                     // 将新获取的数据push到vue中的data，就会反应到视图中了
    //                     JSON.parse(response.data).stories.forEach(function (val, index) {
    //                         _this.articles.push(val);
    //                         // console.log(val);
    //                     });
    //                     // 数据更新完毕，将开关打开
    //                     sw = true;
    //                 })
    //                 .catch(function (error) {
    //                     console.log(error);
    //                 });
    //         }
    //         }
    //     })
    },
    methods:{
        news_in_followed:function(obj) {
            // alert(obj);
            var i = this.user_follow_news.length;
            while (i--) {
                if (this.user_follow_news[i] == obj) {
                    return true;
                }
            }
            return false;
        },
        // 请求分类的新闻
        category_news:function (category_id) {
            // var num = this.getAttribute('category_num');
            // alert(num);
            // alert(typeof(num));
            //    发起请求
            axios.get(this.host+'/news?cate='+category_id,{
                responseType:'json'
            })
            .then(response => {
                this.news = response.data;
                this.current = category_id;
                // console.log(response);
            })
            .catch(error => {
                console.log(error);
            })
        },
        // 请求热门分类的新闻
        category_hot_news:function () {
            //    发起请求
            axios.get(this.host+'/news/',{
                responseType:'json'
            })
            .then(response => {
                this.news = response.data;
                this.current = -1;
                // console.log(response);
            })
            .catch(error => {
                console.log(error);
            })
        },
        // 点击关注新闻
        followed_news:function(news_id){
            axios.post(this.host+'/follows/', {
                    news: news_id,
                    user: this.user_id
            }, {
                headers: {
                'Authorization': 'JWT ' + this.token
            },
                responseType: 'json',
                withCredentials: true
            })
            .then(response =>{
                alert("关注成功!");
                for (var i=0;i<this.news.length;i++){
                    if (this.news[i].id == news_id){
                        Vue.set(this.news[i],"is_followed",true)
                    }
                }
            })
            .catch(error => {
                alert("已关注请勿重新关注!");
                console.log(error)
            })
        },
        // 点击新闻跳转到详情页
    },
})
